function filter(arr, func) {
    let new_arr = [];
    for (let index = 0; index < arr.length; index++) {
        if (func(arr[index],index)) {
            new_arr.push(arr[index]);
        }
    }
    return new_arr;
}

export { filter };
