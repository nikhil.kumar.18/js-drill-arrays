function map(arr, func) {
    let new_arr = []

    for (let index = 0; index < arr.length; index++) {
        new_arr.push(func(arr[index],index));
    }

    return new_arr;
}
export { map };
