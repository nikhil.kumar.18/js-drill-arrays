function reduce(arr, func, starting_value) {
    if (starting_value === undefined) {
        starting_value = arr[0];
        for (let index = 1; index < arr.length; index++) {
            starting_value = func(starting_value,arr[index],index);
        }
    }
    else {
        for (let index = 0; index < arr.length; index++) {
            starting_value = func(starting_value,arr[index],index);
        }

    }


    return starting_value;
}

export { reduce };
