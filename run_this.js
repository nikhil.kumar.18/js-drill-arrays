import { testEach } from './test/testEach.js';
import { testReduce } from './test/testReduce.js';
import { testMap } from './test/testMap.js';
import { testFilter } from './test/testFilter.js';
import { testFind } from './test/testFind.js';
import { testFlatten } from './test/testFlatten.js';
console.log("Testing Functions :-")
console.log("");
testEach();
console.log("");
testReduce();
console.log("");
testMap();
console.log("");
testFilter();
console.log("");
testFind();
console.log("");
testFlatten();
console.log("");


