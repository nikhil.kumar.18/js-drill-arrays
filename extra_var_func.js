const items = [1, 2, 3, 4, 5, 5];

const arr = ["Steve", "Martin", "Dave", "Sid", "Dev"];

const flat = [1, [2], [[3]], [[[4]]]];

const flat2 = [3, [3, [4]], [[4]]];

function print_num(val, index) {
    return val;
}

function iseven(val, index) {
    return val % 2 === 0;
}

function short_length(str,index) {
    return str.length <= 3;
}

function square(value,index) {
    return value * value;
}

function sum(val1, val2,index) {
    return val1 + val2;
}

function mul(val1, val2,index) {
    return val1 * val2;
}

function isdivby3(val,index) {
    return val % 3 == 0;
}

function iseven2(val,index) {
    return val % 2 === 0;
}

function equal(arr1, arr2) {
    if (arr1.length != arr2.length)
        return false;
    for (let index = 0; index < arr1.length; index++) {
        if (arr1[index] != arr2[index])
            return false;
    }
    return true;
}

export { items, iseven, print_num, short_length, equal, flat2, iseven2, square, arr, flat, sum, mul, isdivby3 };
