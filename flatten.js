function flatten(arr) {
    let new_arr = [];
    for (let index = 0; index < arr.length; index++) {
        if (Array.isArray(arr[index])) {
            new_arr = [...new_arr, ...flatten(arr[index])];
            
        }
        else {
            new_arr.push(arr[index]);
        }
    }
    return new_arr;
}

export { flatten };
