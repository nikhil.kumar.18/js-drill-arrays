import { items, iseven2, square, isdivby3, arr, short_length, equal } from '../extra_var_func.js';
import { map } from '../map.js';
function testMap() {
    console.log("Testing Map function:-");
    console.log("------------------------------");
    console.log("Input being Tested:- ",items," Funciton used:- ",square,"\n");
    console.log("Your Output:- ",map(items,square),"\n");
    console.log("Expected Output:- ",[1,4,9,16,25,25],"\n");
    equal(map(items, square), [1, 4, 9, 16, 25, 25]) ? console.log("Test 1 For Map Complete") : console.log("Errors Encountered in Test 1 for Map");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",iseven2,"\n");
    console.log("Your Output:- ",map(items,iseven2),"\n");
    console.log("Expected Output:- ",[false,true,false,true,false,false],"\n");
    equal(map(items, iseven2), [false, true, false, true, false, false]) ? console.log("Test 2 For Map Complete") : console.log("Errors Encountered in Test 2 for Map");
    console.log("");
    console.log("Input being Tested:- ",arr," Funciton used:- ",short_length,"\n");
    console.log("Your Output:- ",map(arr,short_length),"\n");
    console.log("Expected Output:- ",[false,false,false,false,true,true],"\n");
    equal(map(arr, short_length), [false, false, false, true, true]) ? console.log("Test 3 For Map Complete") : console.log("Errors Encountered in Test 3 for Map");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",isdivby3,"\n");
    console.log("Your Output:- ",map(items,isdivby3),"\n");
    console.log("Expected Output:- ",[false,false,true,false,false,false],"\n");
    equal(map(items, isdivby3), [false, false, true, false, false, false]) ? console.log("Test 4 For Map Complete") : console.log("Errors Encountered in Test 4 for Map");
    console.log("------------------------------");
}

export { testMap };
