import { reduce } from '../reduce.js';
import { items, sum, mul } from '../extra_var_func.js';
function testReduce() {
    console.log("Testing Reduce Function:-");
    console.log("------------------------------");
    console.log("Input being Tested:- ",items," Funciton used:- ",sum,"\n");
    console.log("Your Output:- ",reduce(items,sum,undefined),"\n");
    console.log("Expected Output:- ",20,"\n");
    reduce(items, sum, undefined) === 20 ? console.log("Test 1 for Reduce Complete \n") : console.log("Error Encountered in Reduce Test 1");
    console.log("Input being Tested:- ",items," Funciton used:- ",sum,"Starting value:-",3,"\n");
    console.log("Your Output:- ",reduce(items,sum,3),"\n");
    console.log("Expected Output:- ",23,"\n");
    reduce(items, sum, 3) === 23 ? console.log("Test 2 for Reduce Complete \n") : console.log("Error Encountered in Reduce Test 2");
    console.log("Input being Tested:- ",items," Funciton used:- ",mul,"Starting value:-",0,"\n");
    console.log("Your Output:- ",reduce(items,mul,0),"\n");
    console.log("Expected Output:- ",0,"\n");
    reduce(items, mul, 0) === 0 ? console.log("Test 3 for Reduce Complete \n") : console.log("Error Encountered in Reduce Test 3");
    console.log("Input being Tested:- ",items," Funciton used:- ",mul,"Starting value:-",2,"\n");
    console.log("Your Output:- ",reduce(items,mul,2),"\n");
    console.log("Expected Output:- ",1200,"\n");
    reduce(items, mul, 2) === 1200 ? console.log("Test 4 for Reduce Complete \n") : console.log("Error Encountered in Reduce Test 4");
    console.log("------------------------------");
}
export { testReduce };
