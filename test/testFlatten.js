import { flatten } from '../flatten.js';
import { flat, flat2, equal } from '../extra_var_func.js';
function testFlatten() {
    console.log("Testing Flatten Function:-")
    console.log("------------------------------");
    console.log("Input being Tested:- ",flat,"\n");
    console.log("Your Output:- ",flatten(flat),"\n");
    console.log("Expected Output:- ",[1,2,3,4],"\n");
    equal(flatten(flat), [1, 2, 3, 4]) ? console.log("Test 1 for Flatten Complete") : console.log("Errors Encountered for Test 1 in Flatten");
    console.log("");
    console.log("Input being Tested:- ",flat2,"\n");
    console.log("Your Output:- ",flatten(flat2),"\n");
    console.log("Expected Output:- ",[3,3,4,4],"\n");
    equal(flatten(flat2), [3, 3, 4, 4]) ? console.log("Test 2 for Flatten Complete") : console.log("Errors Encountered for Test 2 in Flatten");
    console.log("------------------------------");
}
export { testFlatten };
