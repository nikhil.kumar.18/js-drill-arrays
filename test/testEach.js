import { each } from '../each.js';
import { items, iseven, print_num } from '../extra_var_func.js';

function testEach() {
    console.log("Testing Each Function:-");
    console.log("------------------------------");
    console.log("Input being Tested:- ",items," Funciton used:- ",iseven,"\n");
    each(items, iseven) == undefined ? console.log("Test 1 for Each Complete") : console.log("Error in Test 1 for Each");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",print_num,"\n");
    each(items, print_num) === undefined ? console.log("Test 2 for Each Complete") : console.log("Error in Test 2 for Each");
    console.log("");
    console.log("Input being Tested:- ",2," Funciton used:- ",iseven,"\n");
    each(2, iseven) === true ? console.log("Test 3 for Each Complete") : console.log("Error in Test 3 for Each");
    console.log("------------------------------");
}

export { testEach };
