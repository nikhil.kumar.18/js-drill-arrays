import { find } from '../find.js';
import { items, arr, isdivby3, short_length } from '../extra_var_func.js';
function testFind() {
    console.log("Testing Find Function:-");
    console.log("------------------------------");
    console.log("Input being Tested:- ",items," Funciton used:- ",isdivby3,"\n");
    console.log("Your Output:- ",find(items,isdivby3),"\n");
    console.log("Expected Output:- ",3,"\n");
    find(items, isdivby3) === 3 ? console.log("Test 1 For Find Complete") : console.log("Errors Encountered in Test 1 of Find");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",short_length,"\n");
    console.log("Your Output:- ",find(items,short_length),"\n");
    console.log("Expected Output:- ",undefined,"\n");
    find(items, short_length) === undefined ? console.log("Test 2 For Find Complete") : console.log("Errors Encountered in Test 2 of Find");
    console.log("");
    console.log("Input being Tested:- ",arr," Funciton used:- ",short_length,"\n");
    console.log("Your Output:- ",find(arr,short_length),"\n");
    console.log("Expected Output:- ",'Sid',"\n");
    find(arr, short_length) === 'Sid' ? console.log("Test 3 For Find Complete") : console.log("Errors Encountered in Test 3 of Find");
    console.log("");
    console.log("Input being Tested:- ",arr," Funciton used:- ",isdivby3,"\n");
    console.log("Your Output:- ",find(arr,isdivby3),"\n");
    console.log("Expected Output:- ",undefined,"\n");
    find(arr, isdivby3) === undefined ? console.log("Test 4 For Find Complete") : console.log("Errors Encountered in Test 4 of Find");
    console.log("------------------------------");
}
export { testFind };
