import { filter } from '../filter.js';
import { items, iseven2, isdivby3, arr, short_length, equal } from '../extra_var_func.js';
function testFilter() {
    console.log("Testing Filter Function:-");
    console.log("------------------------------");
    console.log("Input being Tested:- ",items," Funciton used:- ",iseven2,"\n");
    console.log(`Your Output:- `,filter(items,iseven2));
    console.log("");
    console.log(`Expected Output:- `,[2,4]);
    console.log("");
    equal(filter(items, iseven2), [2, 4]) ? console.log("Test 1 for Filter Complete") : console.log("Errors Encountered in Test 1 of Filter");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",isdivby3,"\n");
    console.log(`Your Output:- `,filter(items,isdivby3));
    console.log("");
    console.log(`Expected Output:- `,[3]);
    console.log("");
    equal(filter(items, isdivby3), [3]) ? console.log("Test 2 for Filter Complete") : console.log("Errors Encountered in Test 2 of Filter");
    console.log("");
    console.log("Input being Tested:- ",arr," Funciton used:- ",short_length,"\n");
    console.log(`Your Output:- `,filter(arr,short_length));
    console.log("");
    console.log(`Expected Output:- `,['Sid','Dev']);
    console.log("");
    equal(filter(arr, short_length), ['Sid', 'Dev']) ? console.log("Test 3 for Filter Complete") : console.log("Errors Encountered in Test 3 of Filter");
    console.log("");
    console.log("Input being Tested:- ",items," Funciton used:- ",short_length,"\n");
    console.log(`Your Output:- `,filter(items,short_length));
    console.log("");
    console.log(`Expected Output:- `,[]);
    console.log("");
    equal(filter(items, short_length), []) ? console.log("Test 4 for Filter Complete") : console.log("Errors Encountered in Test 4 of Filter");
    console.log("------------------------------");
}

export { testFilter };
