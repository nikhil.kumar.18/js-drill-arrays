function each(arr, func) {
    if (!Array.isArray(arr)) {

        return true; //returning true because each always returns undefined, in this case i can know that this value is wrong as it is not an array
    }

    for (let index = 0; index < arr.length; index++) {
        func(arr[index], index);
    }
    
}

export { each };
