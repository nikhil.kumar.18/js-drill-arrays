function find(arr, func) {
    for (let index = 0; index < arr.length; index++) {
        if (func(arr[index],index)) {
            return arr[index];
        }
    }
    
}

export { find };
